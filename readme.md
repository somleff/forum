## Commit history

1. Create with UserFactory dummy data for our tables in DB.  
   User tables data filled automatically (check UserFactory.php).  
  
2. Let's write test for "/threads" and "/threads/{id}".  

3. A Thread Can Have Replies. First Unit test in project.  

4. A User May Respond to Threads.  
    Given we have a authenticated user and an existing thread when the user adds a reply to the thread then their reply should be visible on the page. Submit a json request. 

5. The Reply Form.  
    - Create a simple form for add replies to the thread.  
    - Add link "All Threads" to navbar.  
    - Change "brand name" link.  
  
6. A User May Publish Threads.  
    - This feature logic descripted in CreateThreadsTest.  
    - Fill "store" method and add "construct" in ThreadsController.php.  

7. Make Some Testing Helpers.  
    - Create helper functions "create" and "make" in tests/utilities/functions.php.  
    - Create helper method "signIn" in TestCase.php.  
    - Rewrite all previous code constraction with this helpers in all our Tests.  

8. The Exception Handling and Form to create a thread.  
    - Make a form to create a thread.  
    - We can throw Exception manually in our Tests. For this use "with Exception Handling" method.  

9. A Thread Should Be Assigned a Channel (AKA Category).  
    - Create Channel Model.  
    - Create a migration for channels table.  
    - Make relations between Threads and Channel. Threads belongs to Channel and associated with.  

10. Test Validation Errors.  
    - Add validation in "store" method in ThreadsController and write test for it.  
    - Add validation in "store" method in RepliesController and write test for it.  

11. Users Can Filter Threads By Channel.  
    - Add new route. We can filter all threads according to a given channel.  
    - Add to "index" method on ThreadsController "if-else" by channel.  

12. Validation Errors and Old Data.  
    - Add link "New Thread" to navbar.  
    - Diplayed validation errors on create thread form.  
    - Add "Choose a Channel" to create thread form.  
    - Made all field required in create thread form.  

13. Extracting to View Composers.  
    - Refactoring 'App\Channel::all()' query in views file.  

14. A User Can Filter All Threads By Username.  
    - Wrote th test and add hardcoding-link to user threads.  

15. A Lesson in Refactoring.  
    - Clean up the method("index") in ThreadController. Create method("getThread") and put some logic down here.  
    - Create a ThreadsFilter.php for future filtering.  
    - 😳 not exactly understand what Jeff doing but it's ok, keep going 🤔.  

16. Meta Details and Pagination.  
    - Add SideBar with Meta Information ('threads/show.blade.php').  
    - Write a method ('boot') in Thread.php for counting replies.  
    - Add a pagination to replies in method 'snow' in ThreadsController.php.  

17. A User Can Filter Threads By Popularity.  
    - Added quantity of replies into All Threads page.  
    - Added link and working logic for filtering threads by popularity.  
    
18. A User Can Favorite Replies.  
    - Created a new table "favorites" and method ('store') in FavoritesController.php.  
    - Added a new "morphMany" relations in Reply.php on method ('favorites').  
    - Check out a method('an_authenticated_user_may_only_favorite_a_reply_once') in FavoritesTest.php and break up your brain 😉.  

19. The Favorite Button.  
    - Created a button('Like') with count in threads/show.blade.php.  
    - In Reply.php@isFavorited added condition if button('Like') already was clicked the button will be displayed 'disable'.  
    
