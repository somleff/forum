<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class CreateThreadsTest extends TestCase
{
    use DatabaseMigrations;


    /** @test */
    public function an_authenticated_user_can_create_new_forum_threads()
    {
        // Given we have a signed in user
        //$this->actingAs(factory('App\User')->create());
        //
        // To make this line more simple we create a helper.
        // This helper registed in composer.json autoload_dev "files" 
        // and store in tests/utilities/functions.php.
        //
        // $this->actingAs(create('App\User'));
        //
        // But also we can do it like this, to create a new user in a future tests.
        // For this purpose we create "signIn" method in TestCase.php
        $this->signIn();

        // When we hit endpoint to create a new thread
        // $thread = factory('App\Thread')->make();
        //
        // To make this line more simple we create a helper.
        // This helper registed in composer.json autoload_dev "files".
        //
        // !!! We do this techniq in all feature test. !!!
        $thread = make('App\Thread');

        $responce = $this->post('/threads', $thread->toArray());

        // When we visit the thread page we should see the new thread
        $this->get($responce->headers->get('Location'))
            ->assertSee($thread->title)->assertSee($thread->body);
    }


    /** @test */
    public function guest_may_not_create_new_threads()
    {
        // With Exception Handling turn on.
        $this->withExceptionHandling();

        // If quest get to Create Page, guest reddirect to Login Page.
        $this->get('/threads/create')->assertRedirect('/login');

        // If guest manually try submit to Threads URL, once again reddirect.
        $this->post('/threads')->assertRedirect('/login');
    }



    /** @test */
    public function a_thread_requires_a_title()
    {
        // If we try to submit and title of tread is null, we expect an error.
        $this->publishThread(['title' => null])->assertSessionHasErrors('title');
    }


        /** @test */
        public function a_thread_requires_a_body()
        {
            // If we try to submit and body of thread is null, we expect an error.
            $this->publishThread(['body' => null])->assertSessionHasErrors('body');
        }


            /** @test */
            public function a_thread_requires_a_valid_channel()
            {
                // We have only 2 type of channels in our DB.
                factory('App\Channel', 2)->create();

                // If we try to submit and channel_id is null, we expect an error.
                $this->publishThread(['channel_id' => null])
                    ->assertSessionHasErrors('channel_id');

                $this->publishThread(['channel_id' => 999])
                    ->assertSessionHasErrors('channel_id');
            }


                public function publishThread($overrides = [])
                {
                    $this->withExceptionHandling()->signIn();

                    $thread = make('App\Thread', $overrides);

                    return $this->post('/threads', $thread->toArray());
                }
}
